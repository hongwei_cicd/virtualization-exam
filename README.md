## Introduction

- used *mkpasswd* to set up a secret password (louis)

``` mkpasswd -m sha-512 ```

- set up cloud config file 

- used *base64* to encode the cloud-config file and save to file base64cloudconfig.txt

- set up VM on the Vsphere

- configurated RAM from 1G to 512mb

- the result is listed below:

![Result](https://gitlab.com/hongwei_cicd/virtualization-exam/-/raw/main/Screenshot1.png "result")

- run virt-host-validate

![Result2](https://gitlab.com/hongwei_cicd/virtualization-exam/-/raw/main/Screenshot2.png "result2")

- added an internet interface van 554

![Result3](https://gitlab.com/hongwei_cicd/virtualization-exam/-/raw/main/Screenshot3.png "Result3")
